﻿using Bumble.Data;
using Bumble.WebClient.Testing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Bumble.Playground
{
    class Program
    {
        static void Main(string[] args)
        {
            /* using (BumbleDBEntities db = new BumbleDBEntities())
             {
                 db.Settings.Add(new Setting { Key = "Horse", Value = "Hi Hi" });

                 db.SaveChanges();
             }

             using (BumbleDBEntities db = new BumbleDBEntities())
             {
                 foreach (var a in db.Settings)
                 {
                     Console.WriteLine("{0}: {1}", a.Key, a.Value);
                 }

             }*/

            var a = new TestingServer();

            a.Start();

            Console.Write("Server Started...");

            Console.ReadLine();
        }
    }
}
