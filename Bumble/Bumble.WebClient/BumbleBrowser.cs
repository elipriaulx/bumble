﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Bumble.WebClient
{
    public class BumbleBrowser
    {
        private const string _currentNamespace = "Bumble.WebClient.root";

        public bool ExceptionOnError { get; set; }
        public bool ExceptionOnNotFound { get; set; }

        public BumbleBrowser()
        {
            ExceptionOnError = false;
            ExceptionOnNotFound = false;
        }

        public HttpResponseMessage Get(string uri)
        {
            var response = new HttpResponseMessage();

            // Get content
            try
            {
                // Restructure URI
                uri = string.Format("{0}.{1}", _currentNamespace, Uri2Namespace(uri));
                
                Assembly assembly = Assembly.GetExecutingAssembly();

                using (Stream fileContent = assembly.GetManifestResourceStream(uri))
                {
                    StreamReader reader = new StreamReader(fileContent, Encoding.ASCII);
                    response.Content = new StringContent(reader.ReadToEnd());
                }

                // Determine Filetype
                response.Content.Headers.ContentType = GetMediaType(uri.Split('.').Last());
            }
            catch (Exception e)
            {
                if (e is ArgumentNullException || e is ArgumentOutOfRangeException)
                {
                    // The file wasn't found.
                    return FileNotFoundResponse();
                }

                // Some other error occured. 
                return GenericServerErrorResponse();
            }
            
            return response;
        }

        #region Helpers

        protected static string Uri2Namespace(string uri)
        {
            // Ensure length makes sense
            if (uri == string.Empty || uri.Length < 4) throw new ArgumentOutOfRangeException("File name is too short.");

            // Convert to namespace syntax
            uri = uri.Replace('/', '.');

            // Remove leading dot if present
            if (uri[0] == '.') uri = uri.Substring(1);

            // Ensure 2-4 letter file extention. 
            var parts = uri.Split('.');

            if (!(parts.Count() > 1 && parts.Last().Length <= 4 && parts.Last().Length >= 2))
            {
                throw new ArgumentOutOfRangeException("File extention is invalid.");
            }

            return uri;
        }

        protected HttpResponseMessage FileNotFoundResponse()
        {
            if (ExceptionOnNotFound)
            {
                throw new FileNotFoundException("The requested file was not found.");
            }
            else
            {
                var response = new HttpResponseMessage();
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                return response;
            }
        }

        protected HttpResponseMessage GenericServerErrorResponse()
        {
            if (ExceptionOnError)
            {
                throw new FileNotFoundException("A Server error occured.");
            }
            else
            {
                var response = new HttpResponseMessage();
                response.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                return response;
            }
        }

        protected static MediaTypeHeaderValue GetMediaType(string ext)
        {
            ext = ext.Replace(".", "").ToLower().Trim();

            switch (ext)
            {
                case "bmp":
                    return new MediaTypeHeaderValue("image/bmp");

                case "css":
                    return new MediaTypeHeaderValue("text/css");

                case "htm":
                case "html":
                    return new MediaTypeHeaderValue("text/html");

                case "ico":
                    return new MediaTypeHeaderValue("image/x-icon");

                case "jpg":
                    return new MediaTypeHeaderValue("image/jpeg");

                case "js":
                    return new MediaTypeHeaderValue("application/javascript");

                case "pdf":
                    return new MediaTypeHeaderValue("application/pdf");

                case "png":
                    return new MediaTypeHeaderValue("image/png");

                case "txt":
                    return new MediaTypeHeaderValue("text/plain");

                default:
                    throw new ArgumentException("Extention of unsupported type.");
            }
        }

        #endregion
    }
}
