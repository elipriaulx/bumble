﻿using Newtonsoft.Json;
using System.Web.Http;
using System.Web.Http.SelfHost;

namespace Bumble.WebClient.Testing
{
    public class TestingServer
    {
        private HttpSelfHostServer _hostServer;
        private int serverPort = 9999;
        
        public int ServerPort { get { return serverPort; } }
        

        public TestingServer()
        {

        }
        
        public void Start()
        {
            var config = new HttpSelfHostConfiguration(string.Format("http://localhost:{0}", serverPort));

            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
    name: "DefaultApi",
    routeTemplate: "{controller}/{action}/{*uri}",
    defaults: new { id = RouteParameter.Optional });

            config.MaxReceivedMessageSize = 2147483647; // set max datagram size

            //Configure serializers
            var json = config.Formatters.JsonFormatter;
            json.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.Objects;
            json.SerializerSettings.TypeNameHandling = TypeNameHandling.Auto;

            config.Formatters.Remove(config.Formatters.XmlFormatter);

            _hostServer = new HttpSelfHostServer(config);
            _hostServer.OpenAsync().Wait();
        }

        public void StopWebHost()
        {
            try
            {
                _hostServer.CloseAsync().Wait();
                _hostServer.Dispose();
                _hostServer = null;
            }
            catch
            {
            }
        }
    }
}
