﻿using System.Net.Http;
using System.Web.Http;

namespace Bumble.WebClient.Testing.Controllers
{
    public class TestController : ApiController
    {
        static BumbleBrowser client;

        public TestController()
        {
            client = new BumbleBrowser();
        }

        [HttpGet]
        [ActionName("testapi")]
        public string TestApi()
        {
            return "It Works!";
        }

        [HttpGet]
        [ActionName("get")]
        public HttpResponseMessage Get(string uri)
        {
            return client.Get(uri);
        }
    }
}
