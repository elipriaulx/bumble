//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Bumble.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class HiveWorker
    {
        public long Id { get; set; }
        public long HiveId { get; set; }
        public string Address { get; set; }
        public System.Guid Key { get; set; }
        public string Role { get; set; }
        public bool Deleted { get; set; }
    
        public virtual Hive Hive { get; set; }
    }
}
